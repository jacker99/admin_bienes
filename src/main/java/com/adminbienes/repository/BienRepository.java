package com.adminbienes.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.adminbienes.entity.Bien;
import com.adminbienes.entity.Tipo;

/**
 * Interface que extiende del crud para la entidad Bien - Create - Read - Update
 * - Delete Suministrador por el repositorio de Spring
 * 
 * @author Hacker
 *
 */
@Repository
public interface BienRepository extends CrudRepository<Bien, Integer> {

	/**
	 * Metodo que consulta el activo por el tipo que indica el usuario
	 * 
	 * @param id_tipo
	 * @return Devuelve una lista de bienes
	 */
	public List<Bien> findByTipo(Tipo id_tipo);

	/**
	 * Metodo que consulta los activos comprados en una fecha especificada por el
	 * usuario
	 * 
	 * @param fecha_compra
	 * @return Devuelve una lista de Bienes
	 */
	public List<Bien> findByFechaCompra(Date fecha_compra);

	/**
	 * Metodo que consulta el activo que coincida con el serial ingresado por el
	 * usuario
	 * 
	 * @param serial
	 * @return Devuelve un objeto de tipo Bien
	 */
	public Bien findBySerial(String serial);

}
