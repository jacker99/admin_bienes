package com.adminbienes.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.adminbienes.entity.Persona;

/**
 * Interface que extiende del crud para la entidad Persona
 * - Create
 * - Read
 * - Update
 * - Delete 
 * Suministrador por el repositorio de Spring 
 * @author Hacker
 *
 */
@Repository
public interface PersonaRepository extends CrudRepository<Persona,Short> {

}
