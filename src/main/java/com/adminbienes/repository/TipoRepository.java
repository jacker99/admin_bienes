package com.adminbienes.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.adminbienes.entity.Tipo;

/**
 * Interface que extiende del crud para la entidad Tipo
 * - Create
 * - Read
 * - Update
 * - Delete 
 * Suministrador por el repositorio de Spring 
 * @author Hacker
 *
 */
@Repository
public interface TipoRepository extends CrudRepository<Tipo,Short> {

}
