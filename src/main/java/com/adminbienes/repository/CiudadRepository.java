package com.adminbienes.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.adminbienes.entity.Ciudad;

/**
 * Interface que extiende del crud para la entidad Ciudad
 * - Create
 * - Read
 * - Update
 * - Delete 
 * Suministrador por el repositorio de Spring 
 * @author Hacker
 *
 */
@Repository
public interface CiudadRepository extends CrudRepository<Ciudad,Short> {

}
