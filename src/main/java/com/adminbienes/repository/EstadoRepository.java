package com.adminbienes.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.adminbienes.entity.Estado;

/**
 * Interface que extiende del crud para la entidad Estado
 * - Create
 * - Read
 * - Update
 * - Delete 
 * Suministrador por el repositorio de Spring 
 * @author Hacker
 *
 */
@Repository
public interface EstadoRepository extends CrudRepository<Estado,Short> {

}
