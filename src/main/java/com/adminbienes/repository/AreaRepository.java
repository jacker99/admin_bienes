package com.adminbienes.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.adminbienes.entity.Area;

/**
 * Interface que extiende del crud para la entidad Area
 * - Create
 * - Read
 * - Update
 * - Delete 
 * Suministrador por el repositorio de Spring 
 * @author Hacker
 *
 */
@Repository
public interface AreaRepository extends CrudRepository<Area,Short> {

}
