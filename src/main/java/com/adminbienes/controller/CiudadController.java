package com.adminbienes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adminbienes.entity.Ciudad;
import com.adminbienes.service.CiudadService;

/**
 * Controlador del API para la entidad Ciudad en el que se exponen las rutas para
 * su posterior consumo Tambien se maneja la logica del negocio
 * 
 * @author Hacker
 *
 */
@Controller
public class CiudadController {

	@Autowired
	private CiudadService ciudadService;

	@RequestMapping(value = "/ciudad", method = RequestMethod.GET)
	@ResponseBody
	public Object index() {
		return ciudadService.findAll();
	}

	@RequestMapping(value = "/create-ciudad", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String create(@RequestBody Ciudad ciudad) {
		String ciudadId = "";
		try {
			ciudadService.save(ciudad);
			ciudadId = String.valueOf(ciudad.getId());
		} catch (Exception ex) {
			return "Error al crear la ciudad: " + ex.toString();
		}
		return "La ciudad se ha creado exitosamente con el ID = " + ciudadId;
	}

	@RequestMapping("/delete-ciudad/{id}")
	@ResponseBody
	public String delete(@PathVariable Short id) {
		try {
			Ciudad ciudad = ciudadService.findById(id);
			ciudadService.delete(ciudad);
		} catch (Exception ex) {
			return "Error eliminando la ciudad:" + ex.toString();
		}
		return "La ciudad ha sido eliminada exitosamente!";
	}

	@RequestMapping("/update-ciudad/{id}")
	@ResponseBody
	public String updateUser(@RequestBody Ciudad ciudad, @PathVariable Short id) {
		try {
			ciudad.setId(id);
			ciudadService.save(ciudad);
		} catch (Exception ex) {
			return "Error actualizando la ciudad: " + ex.toString();
		}
		return "La ciudad ha sido actualizada exitosamente!";
	}

}
