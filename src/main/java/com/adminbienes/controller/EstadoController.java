package com.adminbienes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adminbienes.entity.Estado;
import com.adminbienes.service.EstadoService;

/**
 * Controlador del API para la entidad Estado en el que se exponen las rutas para
 * su posterior consumo Tambien se maneja la logica del negocio
 * 
 * @author Hacker
 *
 */
@Controller
public class EstadoController {

	@Autowired
	private EstadoService estadoService;

	@RequestMapping(value = "/estado", method = RequestMethod.GET)
	@ResponseBody
	public Object index() {
		return estadoService.findAll();
	}

	@RequestMapping(value = "/create-estado", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String create(@RequestBody Estado estado) {
		String estadoId = "";
		try {
			estadoService.save(estado);
			estadoId = String.valueOf(estado.getId());
		} catch (Exception ex) {
			return "Error creando el estado: " + ex.toString();
		}
		return "El estado ha sido creado exitosamente con el ID = " + estadoId;
	}

	@RequestMapping("/delete-estado/{id}")
	@ResponseBody
	public String delete(@PathVariable Short id) {
		try {
			Estado estado = estadoService.findById(id);
			estadoService.delete(estado);
		} catch (Exception ex) {
			return "Error elimnando el estado:" + ex.toString();
		}
		return "El estado ha sido eliminado exitosamente!";
	}

	@RequestMapping("/update-estado/{id}")
	@ResponseBody
	public String updateUser(@RequestBody Estado estado, @PathVariable Short id) {
		try {
			estado.setId(id);
			estadoService.save(estado);
		} catch (Exception ex) {
			return "Error actualizando el estado: " + ex.toString();
		}
		return "El estado ha sido actualizado exitosamente!";
	}

}
