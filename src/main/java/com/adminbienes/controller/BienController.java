package com.adminbienes.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adminbienes.entity.Bien;
import com.adminbienes.entity.Tipo;
import com.adminbienes.service.BienService;
import com.adminbienes.service.TipoService;

/**
 * Controlador del API para la entidad Bien en el que se exponen las rutas para
 * su posterior consumo Tambien se maneja la logica del negocio
 * 
 * @author Hacker
 *
 */
@Controller
public class BienController {

	@Autowired
	private BienService bienService;
	@Autowired
	private TipoService tipoService;

	@RequestMapping(value = "/bien", method = RequestMethod.GET)
	@ResponseBody
	public Object index() {
		return bienService.findAll();
	}

	@RequestMapping(value = "/create-bien", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String create(@RequestBody Bien bien) {
		String bienId = "";
		try {
			if (bien.getFechaBaja() != null && !bien.getFechaCompra().before(bien.getFechaBaja())) {
				return "La fecha de baja del bien no puede ser anterior a la fecha de compra. Por favor verifique la información";
			}
			bienService.save(bien);
			bienId = String.valueOf(bien.getId());
		} catch (Exception ex) {
			return "Error creando el bien: " + ex.toString();
		}
		return "El bien ha sido creado exitosamente con el ID = " + bienId;
	}

	@RequestMapping("/delete-bien/{id}")
	@ResponseBody
	public String delete(@PathVariable Integer id) {
		try {
			Bien bien = bienService.findById(id);
			if (bien == null) {
				return "El ID Indicado no existe, por favor verifique la información";
			}
			bienService.delete(bien);

		} catch (Exception ex) {
			return "Error eliminando el bien:" + ex.toString();
		}
		return "El bien ha sido eliminado exitosamente!";
	}

	@RequestMapping("/update-bien/{id}")
	@ResponseBody
	public String updateUser(@RequestBody Bien bien, @PathVariable Integer id) {
		try {
			Bien bienActual = bienService.findById(id);
			if (!bienActual.getFechaCompra().equals(bien.getFechaCompra())) {
				return "La fecha de compra no se puede modificar. Por favor verifique la información ingresada";
			}
			if (bien.getFechaBaja() != null && !bienActual.getFechaCompra().before(bien.getFechaBaja())) {
				return "La fecha de baja del bien no puede ser anterior a la fecha de compra. Por favor verifique la información";
			}
			bien.setId(id);
			bienService.save(bien);
		} catch (Exception ex) {
			return "Error actualizando el bien: " + ex.toString();
		}
		return "El bien ha sido actualizado exitosamente!";
	}

	@RequestMapping("/get-by-serial/{serial}")
	@ResponseBody
	public Object getBySerial(@PathVariable String serial) {
		try {
			Bien bien = bienService.findBySerial(serial);
			if (bien == null) {
				return "No fue posible encontrar ningun activo con ese serial";
			}
			return bien;
		} catch (Exception ex) {
			return "No fue posible encontrar ningun activo con ese serial";
		}
	}

	@RequestMapping("/get-by-dateP/{serial}")
	@ResponseBody
	public Object getByDatePurchase(@PathVariable String serial) {
		SimpleDateFormat format;
		Date parsed;
		try {
			format = new SimpleDateFormat("yyyyMMdd");
			parsed = format.parse(serial);
			java.sql.Date dateSql = new java.sql.Date(parsed.getTime());
			List<Bien> bienes = bienService.findByDatePurchase(dateSql);
			if (bienes == null || bienes.isEmpty()) {
				return "No fue posible encontrar ningun activo con fecha de compra para la fecha indicada";
			}
			return bienes;
		} catch (ParseException e) {
			return "Por favor verifique el formato de su fecha debe ser: yyyyMMdd";
		} catch (Exception ex) {
			return "No fue posible encontrar ningun activo con fecha de compra para la fecha indicada";
		}
	}

	@RequestMapping("/get-by-type/{id_tipo}")
	@ResponseBody
	public Object getByType(@PathVariable Short id_tipo) {
		try {

			Tipo tipo = tipoService.findById(id_tipo);
			List<Bien> bienes = bienService.findByType(tipo);
			if (bienes == null || bienes.isEmpty()) {
				return "No se han encontrado resultados de activos para este tipo de activo";
			}
			return bienes;
		} catch (Exception ex) {
			return "No se han encontrado resultados de activos para este tipo de activo";
		}
	}

}
