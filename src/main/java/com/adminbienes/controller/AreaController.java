package com.adminbienes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adminbienes.entity.Area;
import com.adminbienes.service.AreaService;

/**
 * Controlador del API para la entidad Area en el que se exponen las rutas para
 * su posterior consumo Tambien se maneja la logica del negocio
 * 
 * @author Hacker
 *
 */
@Controller
public class AreaController {

	@Autowired
	private AreaService areaService;

	@RequestMapping(value = "/area", method = RequestMethod.GET)
	@ResponseBody
	public Object index() {
		return areaService.findAll();
	}

	@RequestMapping(value = "/create-area", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String create(@RequestBody Area area) {
		String areaId = "";
		try {
			areaService.save(area);
			areaId = String.valueOf(area.getId());
		} catch (Exception ex) {
			return "Error creando el Area : " + ex.toString();
		}
		return "El area ha sido creada exitosamente con el ID = " + areaId;
	}

	@RequestMapping("/delete-area/{id}")
	@ResponseBody
	public String delete(@PathVariable Short id) {
		try {
			Area area = areaService.findById(id);
			areaService.delete(area);
		} catch (Exception ex) {
			return "Error eliminando el area:" + ex.toString();
		}
		return "El area ha sido eliminada exitosamente!";
	}

	@RequestMapping("/update-area/{id}")
	@ResponseBody
	public String updateUser(@RequestBody Area area, @PathVariable Short id) {
		try {
			area.setId(id);
			areaService.save(area);
		} catch (Exception ex) {
			return "Error actualizando el area : " + ex.toString();
		}
		return "El area ha sido actualizada exitosamente!";
	}

}
