package com.adminbienes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adminbienes.entity.Tipo;
import com.adminbienes.service.TipoService;

/**
 * Controlador del API para la entidad Tipo en el que se exponen las rutas para
 * su posterior consumo Tambien se maneja la logica del negocio
 * 
 * @author Hacker
 *
 */
@Controller
public class TipoController {

	@Autowired
	private TipoService tipoService;

	@RequestMapping(value = "/tipo", method = RequestMethod.GET)
	@ResponseBody
	public Object index() {
		return tipoService.findAll();
	}

	@RequestMapping(value = "/create-tipo", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String create(@RequestBody Tipo tipo) {
		String tipoId = "";
		try {
			tipoService.save(tipo);
			tipoId = String.valueOf(tipo.getId());
		} catch (Exception ex) {
			return "Error creando el Tipo : " + ex.toString();
		}
		return "El tipo ha sido creada exitosamente con el ID = " + tipoId;
	}

	@RequestMapping("/delete-tipo/{id}")
	@ResponseBody
	public String delete(@PathVariable Short id) {
		try {
			Tipo tipo = tipoService.findById(id);
			tipoService.delete(tipo);
		} catch (Exception ex) {
			return "Error eliminando el tipo:" + ex.toString();
		}
		return "El tipo ha sido eliminada exitosamente!";
	}

	@RequestMapping("/update-tipo/{id}")
	@ResponseBody
	public String updateUser(@RequestBody Tipo tipo, @PathVariable Short id) {
		try {
			tipo.setId(id);
			tipoService.save(tipo);
		} catch (Exception ex) {
			return "Error actualizando el tipo : " + ex.toString();
		}
		return "El tipo ha sido actualizada exitosamente!";
	}

}
