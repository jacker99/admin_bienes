package com.adminbienes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adminbienes.entity.Persona;
import com.adminbienes.service.PersonaService;

/**
 * Controlador del API para la entidad Persona en el que se exponen las rutas para
 * su posterior consumo Tambien se maneja la logica del negocio
 * 
 * @author Hacker
 *
 */
@Controller
public class PersonaController {

	@Autowired
	private PersonaService personaService;

	@RequestMapping(value = "/persona", method = RequestMethod.GET)
	@ResponseBody
	public Object index() {
		return personaService.findAll();
	}

	@RequestMapping(value = "/create-persona", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String create(@RequestBody Persona persona) {
		String personaId = "";
		try {
			personaService.save(persona);
			personaId = String.valueOf(persona.getId());
		} catch (Exception ex) {
			return "Error creando el Persona : " + ex.toString();
		}
		return "La persona ha sido creada exitosamente con el ID = " + personaId;
	}

	@RequestMapping("/delete-persona/{id}")
	@ResponseBody
	public String delete(@PathVariable Short id) {
		try {
			Persona persona = personaService.findById(id);
			personaService.delete(persona);
		} catch (Exception ex) {
			return "Error eliminando el persona:" + ex.toString();
		}
		return "La persona ha sido eliminada exitosamente!";
	}

	@RequestMapping("/update-persona/{id}")
	@ResponseBody
	public String updateUser(@RequestBody Persona persona, @PathVariable Short id) {
		try {
			persona.setId(id);
			personaService.save(persona);
		} catch (Exception ex) {
			return "Error actualizando el persona : " + ex.toString();
		}
		return "La persona ha sido actualizada exitosamente!";
	}

}
