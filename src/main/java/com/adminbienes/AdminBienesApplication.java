package com.adminbienes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;


@EntityScan(basePackages="com.adminbienes.entity")
@SpringBootApplication
public class AdminBienesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminBienesApplication.class, args);
	}
}
