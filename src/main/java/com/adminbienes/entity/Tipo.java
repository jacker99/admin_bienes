package com.adminbienes.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Tipo")
public class Tipo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8925664753514814603L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Short id;

	@NotNull
	@Size(min = 1, max = 45)
	@Column(name = "descripcion", nullable = false)
	private String descripcion;

	/**
	 * Constructor vacio de la entidad Tipo
	 */
	public Tipo() {
	}

	/**
	 * Constructor con todos los parametros de la entidad Tipo para su instanciacion
	 * 
	 * @param id          Short Maximo 3 Digitos
	 * @param descripcion String Maximo 45 Caracteres
	 */
	public Tipo(Short id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	/**
	 * Obtiene el valor del atributo Id Maximo 3 Digitos
	 * 
	 * @return id Short
	 */
	public Short getId() {
		return id;
	}

	/**
	 * Modifica el valor del atributo Id Maximo 5 Digitos No introducir valores
	 * nulos
	 * 
	 * @param id Short
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * Obtiene el valor del atributo descripcion Maximo 45 Caracteres
	 * 
	 * @return descripcion String
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Modifica el valor del atributo descripcion Maximo 45 Caracteres No introducir
	 * valores nulos
	 * 
	 * @param descripcion String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
