package com.adminbienes.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Bien")
public class Bien implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -882338515649737452L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@NotNull
	@Column(name = "id", nullable = false)
	private Integer id;

	@NotNull
	@Size(min = 1, max = 100)
	@Column(name = "nombre", nullable = false)
	private String nombre;

	@NotNull
	@Size(min = 1, max = 300)
	@Column(name = "descripcion", nullable = false)
	private String descripcion;

	@NotNull
	@Size(min = 1, max = 30)
	@Column(name = "serial", unique = true, nullable = false)
	private String serial;

	@NotNull
	@Column(name = "num_int_inventario", nullable = false)
	private Integer numIntInventario;

	@NotNull
	@Column(name = "peso", nullable = false)
	private Integer peso;

	@NotNull
	@Column(name = "alto", nullable = false)
	private Short alto;

	@NotNull
	@Column(name = "ancho", nullable = false)
	private Short ancho;

	@NotNull
	@Column(name = "largo", nullable = false)
	private Short largo;

	@NotNull
	@Column(name = "valor_compra", nullable = false)
	private BigDecimal valorCompra;

	@NotNull
	@Column(name = "fecha_compra", nullable = false)
	private Date fechaCompra;

	@Column(name = "fecha_baja")
	private Date fechaBaja;

	@NotNull
	@Size(min = 1, max = 30)
	@Column(name = "color", nullable = false)
	private String color;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id_estado")
	private Estado estado;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id_tipo")
	private Tipo tipo;

	@Column(name = "asignacion")
	private Short asignacion;

	/**
	 * Constructor vacio de la entidad Bien
	 */
	public Bien() {
	}

	/**
	 * Constructor con todos los parametros de la entidad Bien
	 * 
	 * @param id                 Integer Maximo 9 Digitos
	 * @param nombre             String Maximo 100 Caracteres
	 * @param descripcion        String Maximo 300 Caracteres
	 * @param serial             String Maximo 30 Caracteres
	 * @param num_int_inventario Maximo 9 Digitos
	 * @param peso               Maximo 9 Digitos
	 * @param alto               Maximo 5 Digitos
	 * @param ancho              Maximo 5 Digitos
	 * @param largo              Maximo 5 Digitos
	 * @param valor_compra       Puede contener decimales
	 * @param fecha_compra       Fecha
	 * @param fecha_baja         Fecha puede ser nula
	 * @param color              Maximo 30 Caracteres
	 * @param estado             Estado
	 * @param tipo               Tipo
	 * @param asignacion         Variable utilizada para saber la asociacion del
	 *                           bien null-No signado 0-Asignado a persona
	 *                           1-Asignado A Area
	 */
	public Bien(Integer id, String nombre, String descripcion, String serial, Integer numIntInventario, Integer peso,
			Short alto, Short ancho, Short largo, BigDecimal valorCompra, Date fechaCompra, Date fechaBaja,
			String color, Estado estado, Tipo tipo, Short asignacion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.serial = serial;
		this.numIntInventario = numIntInventario;
		this.peso = peso;
		this.alto = alto;
		this.ancho = ancho;
		this.largo = largo;
		this.valorCompra = valorCompra;
		this.fechaCompra = fechaCompra;
		this.fechaBaja = fechaBaja;
		this.color = color;
		this.estado = estado;
		this.tipo = tipo;
		this.asignacion = asignacion;
	}

	/**
	 * Obtiene el valor del atributo Id Maximo 9 Digitos
	 * 
	 * @return id Integer
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Modifica el valor del atributo Id Maximo 9 Digitos No introducir valores
	 * nulos
	 * 
	 * @param id Integer
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Obtiene el valor del atributo Nombre String Maximo 100 Caracteres
	 * 
	 * @return nombre String
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Modifica el valor del atributo nombre String Maximo 100 Caracteres No
	 * introducir valores nulos
	 * 
	 * @param nombre String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Obtiene el valor del atributo descripcion Maximo 300 caracteres
	 * 
	 * @return descripcion String
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Modifica el valor del atributo descripcion Maximo 300 caracteres No
	 * introducir valores nulos
	 * 
	 * @return descripcion String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Obtiene el valor del atributo serial Maximo 30 Caracteres
	 * 
	 * @return serial String
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * Modifica el valor del atributo serial Maximo 30 Caracteres No introducir
	 * valores nulos
	 * 
	 * @return serial String
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
	 * Obtiene el valor del atributo peso Maximo 9 Digitos
	 * 
	 * @return peso Integer
	 */
	public Integer getPeso() {
		return peso;
	}

	/**
	 * Modifica el valor del atributo peso Maximo 9 Digitos No introducir valores
	 * nulos
	 * 
	 * @return peso Integer
	 */
	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	/**
	 * Obtiene el valor del atributo alto Maximo 5 Digitos
	 * 
	 * @return alto Short
	 */
	public Short getAlto() {
		return alto;
	}

	/**
	 * Modifica el valor del atributo alto Maximo 5 Digitos No introducir valores
	 * nulos
	 * 
	 * @return alto Short
	 */
	public void setAlto(Short alto) {
		this.alto = alto;
	}

	/**
	 * Obtiene el valor del atributo ancho Maximo 5 Digitos
	 * 
	 * @return ancho Short
	 */
	public Short getAncho() {
		return ancho;
	}

	/**
	 * Modifica el valor del atributo ancho Maximo 5 Digitos No introducir valores
	 * nulos
	 * 
	 * @return ancho Short
	 */
	public void setAncho(Short ancho) {
		this.ancho = ancho;
	}

	/**
	 * Obtiene el valor del atributo largo Maximo 5 Digitos
	 * 
	 * @return largo Short
	 */
	public Short getLargo() {
		return largo;
	}

	/**
	 * Modifica el valor del atributo largo Maximo 5 Digitos No introducir valores
	 * nulos
	 * 
	 * @return largo Short
	 */
	public void setLargo(Short largo) {
		this.largo = largo;
	}

	/**
	 * Obtiene el valor del atributo numIntInventario Maximo 9 Digitos
	 * 
	 * @return numIntInventario Integer
	 */
	public Integer getNumIntInventario() {
		return numIntInventario;
	}

	/**
	 * Modifica el valor del atributo numIntInventario Maximo 9 Digitos No
	 * introducir valores nulos
	 * 
	 * @return numIntInventario Integer
	 */
	public void setNumIntInventario(Integer numIntInventario) {
		this.numIntInventario = numIntInventario;
	}

	/**
	 * Obtiene el valor del atributo valorCompra Maximo 15 Digitos Enteros y 3
	 * Decimales
	 * 
	 * @return valorCompra BigDecimal
	 */
	public BigDecimal getValorCompra() {
		return valorCompra;
	}

	/**
	 * Modifica el valor del atributo valorCompra Maximo 15 Digitos Enteros y 3
	 * Decimales No introducir valores nulos
	 * 
	 * @return valorCompra Integer
	 */
	public void setValorCompra(BigDecimal valorCompra) {
		this.valorCompra = valorCompra;
	}

	/**
	 * Obtiene el valor del atributo fechaCompra
	 * 
	 * @return fechaCompra java.sql.Date
	 */
	public Date getFechaCompra() {
		return fechaCompra;
	}

	/**
	 * Modifica el valor del atributo fechaCompra No introducir valores nulos
	 * 
	 * @return valorCompra java.sql.Date
	 */
	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	/**
	 * Obtiene el valor del atributo fechaBaja Puede ser nulo
	 * 
	 * @return fechaBaja java.sql.Date
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * Modifica el valor del atributo fechaBaja
	 * 
	 * @return valorCompra java.sql.Date
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * Obtiene el valor del atributo color Maximo 30 Caracteres
	 * 
	 * @return color String
	 */
	public String getColor() {
		return color;
	}

	/**
	 * Modifica el valor del atributo color String Maximo 30 Caracteres No
	 * introducir valores nulos
	 * 
	 * @param color String
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * Obtiene el valor del atributo estado - Index FK - id_estado
	 * 
	 * @return estado Estado Object
	 */
	public Estado getEstado() {
		return estado;
	}

	/**
	 * Modifica el valor del atributo estado - Index FK - id_estado No introducir
	 * valores nulos
	 * 
	 * @param estado Estado Object
	 */
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	/**
	 * Obtiene el valor del atributo tipo - Index FK - id_tipo
	 * 
	 * @return tipo Tipo Object
	 */
	public Tipo getTipo() {
		return tipo;
	}

	/**
	 * Modifica el valor del atributo tipo - Index FK - id_tipo No introducir
	 * valores nulos
	 * 
	 * @param tipo Tipo Object
	 */
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	/**
	 * Obtiene el valor del atributo asignacion Permanece null mientras no se
	 * realice una asignacion a una persona o area Si se realiza asignacion a una
	 * persona toma el valor de 0 Si se realiza asignacion a un area toma el valor
	 * de 1
	 * 
	 * @return asignacion String
	 */
	public Short getAsignacion() {
		return asignacion;
	}

	/**
	 * Modifica el valor del atributo asignacion Permanece null mientras no se
	 * realice una asignacion a una persona o area Si se realiza asignacion a una
	 * persona toma el valor de 0 Si se realiza asignacion a un area toma el valor
	 * de 1
	 * 
	 * @param asignacion String
	 */
	public void setAsignacion(Short asignacion) {
		this.asignacion = asignacion;
	}

}
