package com.adminbienes.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Estado")
public class Estado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8436188727987185121L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Short id;

	@NotNull
	@Size(min = 1, max = 15)
	@Column(name = "descripcion", nullable = false)
	private String descripcion;

	/**
	 * Constructor vacio de la entidad Estado
	 */
	public Estado() {
	}

	/**
	 * Constructor con todos los parametros de la entidad Area para su instanciacion
	 * 
	 * @param id          Short Maximo 3 Digitos
	 * @param descripcion String Maximo 15 Caracteres
	 */
	public Estado(Short id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	/**
	 * Obtiene el valor del atributo Id Maximo 3 Digitos
	 * 
	 * @return id Short
	 */
	public Short getId() {
		return id;
	}

	/**
	 * Modifica el valor del atributo Id Maximo 3 Digitos No introducir valores
	 * nulos
	 * 
	 * @param id Short
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * Obtiene el valor del atributo descripcion Maximo 15 Caracteres
	 * 
	 * @return descripcion String
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Modifica el valor del atributo descripcion Maximo 15 Caracteres No introducir
	 * valores nulos
	 * 
	 * @param descripcion String
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
