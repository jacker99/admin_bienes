package com.adminbienes.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Ciudad")
public class Ciudad implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4712415124434108671L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Short id;

	@NotNull
	@Size(min = 1, max = 45)
	@Column(name = "nombre", nullable = false)
	private String nombre;

	/**
	 * Constructor vacio de la entidad Ciudad
	 */
	public Ciudad() {
	}

	/**
	 * Constructor con todos los parametros de la entidad Ciudad para su instanciacion
	 * 
	 * @param id     Short Maximo 4 Digitos
	 * @param nombre String Maximo 45 Caracteres
	 */
	public Ciudad(Short id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	/**
	 * Obtiene el valor del atributo Id Maximo 4 Digitos
	 * 
	 * @return id Short
	 */
	public Short getId() {
		return id;
	}

	/**
	 * Modifica el valor del atributo Id Maximo 4 Digitos No introducir valores
	 * nulos
	 * 
	 * @param id Short
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * Obtiene el valor del atributo nombre Maximo 45 Caracteres
	 * 
	 * @return nombre String
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Modifica el valor del atributo nombre Maximo 45 Caracteres No introducir
	 * valores nulos
	 * 
	 * @param nombre String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
