package com.adminbienes.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Area")
public class Area implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3007725845257794310L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@NotNull
	@Column(name = "id", nullable = false)
	private Short id;

	@NotNull
	@Size(min = 1, max = 45)
	@Column(name = "nombre", nullable = false)
	private String nombre;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id_ciudad")
	private Ciudad ciudad;

	/**
	 * Constructor vacio de la entidad Area
	 */
	public Area() {
	}

	/**
	 * Constructor con todos los parametros de la entidad Area para su instanciacion
	 * 
	 * @param id     Short Maximo 5 Digitos
	 * @param nombre String Maximo 80 Caracteres
	 * @param ciudad Ciudad -- Relacion con la entidad Ciudad
	 */
	public Area(Short id, String nombre, Ciudad ciudad) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.ciudad = ciudad;
	}

	/**
	 * Obtiene el valor del atributo Id Maximo 5 Digitos
	 * 
	 * @return id Short
	 */
	public Short getId() {
		return id;
	}

	/**
	 * Modifica el valor del atributo Id Maximo 5 Digitos No introducir valores
	 * nulos
	 * 
	 * @param id Short
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * Obtiene el valor del atributo nombre Maximo 80 Caracteres
	 * 
	 * @return nombre String
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Modifica el valor del atributo nombre Maximo 80 Caracteres No introducir
	 * valores nulos
	 * 
	 * @param nombre String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Obtiene el valor del atributo Ciudad Relacion con la entidad Ciudad numerico
	 * 
	 * @return ciudad Ciudad
	 */
	public Ciudad getCiudad() {
		return ciudad;
	}

	/**
	 * Modifica el valor del atributo Ciudad Relacion con la entidad Ciudad numerico
	 * No introducir valores nulos
	 * 
	 * @return ciudad Ciudad
	 */
	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

}
