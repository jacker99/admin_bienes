package com.adminbienes.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Bien_Area")
public class BienArea implements Serializable {
	/**
	 * *
	 */

	private static final long serialVersionUID = 16020570315586158L;
	@Id
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id_bien")
	private Bien bien;
	@Id
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id_area")
	private Area area;

	public BienArea() {
	}

	public BienArea(Bien bien, Area area) {
		super();
		this.bien = bien;
		this.area = area;
	}

	public Bien getBien() {
		return bien;
	}

	public void setBien(Bien bien) {
		this.bien = bien;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
}