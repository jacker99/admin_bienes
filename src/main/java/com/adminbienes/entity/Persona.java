package com.adminbienes.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Persona")
public class Persona implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8957178146412828476L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Short id;

	@Size(min = 1, max = 100)
	@Column(name = "nombre", nullable = false)
	private String nombre;

	@Size(min = 1, max = 45)
	@Column(name = "cargo", nullable = false)
	private String cargo;

	/**
	 * Constructor vacio de la entidad Persona
	 */
	public Persona() {
	}

	/**
	 * Constructor con todos los parametros de la entidad Persona para su instanciacion
	 * 
	 * @param id     Short Maximo 5 Digitos
	 * @param nombre String Maximo 100 Caracteres
	 * @param cargo  String -- Maximo 45 Caracteres
	 */
	public Persona(Short id, String nombre, String cargo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.cargo = cargo;
	}

	/**
	 * Obtiene el valor del atributo Id Maximo 5 Digitos
	 * 
	 * @return id Short
	 */
	public Short getId() {
		return id;
	}

	/**
	 * Modifica el valor del atributo Id Maximo 5 Digitos No introducir valores
	 * nulos
	 * 
	 * @param id Short
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * Obtiene el valor del atributo nombre Maximo 100 Caracteres
	 * 
	 * @return nombre String
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Modifica el valor del atributo nombre Maximo 100 Caracteres No introducir
	 * valores nulos
	 * 
	 * @param nombre String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Obtiene el valor del atributo cargo Maximo 45 Caracteres
	 * 
	 * @return cargo String
	 */
	public String getCargo() {
		return cargo;
	}

	/**
	 * Modifica el valor del atributo cargo Maximo 45 Caracteres No introducir
	 * valores nulos
	 * 
	 * @param cargo String
	 */
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

}
