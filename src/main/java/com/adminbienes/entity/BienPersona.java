package com.adminbienes.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Bien_Persona")
public class BienPersona implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 610376617799007499L;

	@Id
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id_bien")
	private Bien bien;

	@Id
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id_persona")
	private Persona persona;

	public BienPersona() {
	}

	public BienPersona(Bien bien, Persona persona) {
		super();
		this.bien = bien;
		this.persona = persona;
	}

	public Bien getBien() {
		return bien;
	}

	public void setBien(Bien bien) {
		this.bien = bien;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
