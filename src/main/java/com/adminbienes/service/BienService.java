package com.adminbienes.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adminbienes.entity.Bien;
import com.adminbienes.entity.Tipo;
import com.adminbienes.repository.BienRepository;

/**
 * Servicio que implementa la interfaz del repositorio con el CRUD y filtros
 * adicionales (Serial, Fecha de Compra y Tipo de Bien) para la entidad Area
 * 
 * @author Hacker
 *
 */
@Service
public class BienService {

	@Autowired
	private BienRepository bienRepository;

	public List<Bien> findAll() {
		return (List<Bien>) bienRepository.findAll();
	}

	public Bien findById(Integer id) {
		return bienRepository.findOne(id);
	}

	public Bien save(Bien bien) {
		return bienRepository.save(bien);
	}

	public void delete(Bien bien) {
		bienRepository.delete(bien);
		return;
	}

	public List<Bien> findByType(Tipo id_tipo) {
		return bienRepository.findByTipo(id_tipo);
	}

	public List<Bien> findByDatePurchase(Date fecha_compra) {
		return bienRepository.findByFechaCompra(fecha_compra);
	}

	public Bien findBySerial(String serial) {
		return bienRepository.findBySerial(serial);
	}

}
