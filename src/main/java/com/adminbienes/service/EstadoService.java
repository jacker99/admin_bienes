package com.adminbienes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adminbienes.entity.Estado;
import com.adminbienes.repository.EstadoRepository;

/**
 * Servicio que implementa la interfaz del repositorio con el CRUD para la
 * entidad Estado
 * 
 * @author Hacker
 *
 */
@Service
public class EstadoService {

	@Autowired
	private EstadoRepository estadoRepository;

	public List<Estado> findAll() {
		return (List<Estado>) estadoRepository.findAll();
	}

	public Estado findById(Short id) {
		return estadoRepository.findOne(id);
	}

	public Estado save(Estado estado) {
		return estadoRepository.save(estado);
	}

	public void delete(Estado estado) {
		estadoRepository.delete(estado);
		return;
	}

}
