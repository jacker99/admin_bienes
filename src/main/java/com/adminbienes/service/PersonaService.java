package com.adminbienes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adminbienes.entity.Persona;
import com.adminbienes.repository.PersonaRepository;

/**
 * Servicio que implementa la interfaz del repositorio con el CRUD para la
 * entidad Persona
 * 
 * @author Hacker
 *
 */
@Service
public class PersonaService {

	@Autowired
	private PersonaRepository personaRepository;

	public List<Persona> findAll() {
		return (List<Persona>) personaRepository.findAll();
	}

	public Persona findById(Short id) {
		return personaRepository.findOne(id);
	}

	public Persona save(Persona persona) {
		return personaRepository.save(persona);
	}

	public void delete(Persona persona) {
		personaRepository.delete(persona);
		return;
	}

}
