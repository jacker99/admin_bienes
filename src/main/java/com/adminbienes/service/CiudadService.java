package com.adminbienes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adminbienes.entity.Ciudad;
import com.adminbienes.repository.CiudadRepository;

/**
 * Servicio que implementa la interfaz del repositorio con el CRUD para la
 * entidad Ciudad
 * 
 * @author Hacker
 *
 */
@Service
public class CiudadService {

	@Autowired
	private CiudadRepository ciudadRepository;

	public List<Ciudad> findAll() {
		return (List<Ciudad>) ciudadRepository.findAll();
	}

	public Ciudad findById(Short id) {
		return ciudadRepository.findOne(id);
	}

	public Ciudad save(Ciudad ciudad) {
		return ciudadRepository.save(ciudad);
	}

	public void delete(Ciudad ciudad) {
		ciudadRepository.delete(ciudad);
		return;
	}

}
