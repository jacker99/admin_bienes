package com.adminbienes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adminbienes.entity.Tipo;
import com.adminbienes.repository.TipoRepository;

/**
 * Servicio que implementa la interfaz del repositorio con el CRUD para la
 * entidad Tipo
 * 
 * @author Hacker
 *
 */
@Service
public class TipoService {

	@Autowired
	private TipoRepository tipoRepository;

	public List<Tipo> findAll() {
		return (List<Tipo>) tipoRepository.findAll();
	}

	public Tipo findById(Short id) {
		return tipoRepository.findOne(id);
	}

	public Tipo save(Tipo tipo) {
		return tipoRepository.save(tipo);
	}

	public void delete(Tipo tipo) {
		tipoRepository.delete(tipo);
		return;
	}

}
