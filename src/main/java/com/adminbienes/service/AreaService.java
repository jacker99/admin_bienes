package com.adminbienes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adminbienes.entity.Area;
import com.adminbienes.repository.AreaRepository;

/**
 * Servicio que implementa la interfaz del repositorio con el CRUD para la
 * entidad Area
 * 
 * @author Hacker
 *
 */
@Service
public class AreaService {

	@Autowired
	private AreaRepository areaRepository;

	public List<Area> findAll() {
		return (List<Area>) areaRepository.findAll();
	}

	public Area findById(Short id) {
		return areaRepository.findOne(id);
	}

	public Area save(Area area) {
		return areaRepository.save(area);
	}

	public void delete(Area area) {
		areaRepository.delete(area);
		return;
	}

}
